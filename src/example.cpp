/**
 * \file ers2idl/src/example.cpp
 * Implementation of IDL interfaces converter and ex_raiser from idl/example.idl
 * Demonstrates use of daq::idl2ers_issue and daq::ers2idl_issue.
 * \author Andrei Kazarov
 * \date Oct. 2023
 *  *
 */

#include "example/example.hh"
#include <ers2idl/ers2idl.h>

// define ERS Issue example::Exception
ERS_DECLARE_ISSUE(  example, 
                        Exception,
		                ERS_EMPTY, ERS_EMPTY
                     )

class Example: POA_example::converter {
        Example() {} ;
        ~Example() {} ;

        // implementation of CORBA interface
        // get idl_issue, convert to ERS, raise and catch, create new one with nested, return to client as out IDL result
        void convert(::ers2idl::Issue_out result, const ::ers2idl::Issue& idl_issue) {
            std::unique_ptr<ers::Issue> ers_issue = daq::idl2ers_issue(idl_issue) ;

            try {
                ers_issue.get()->raise() ;
            } catch ( std::exception & ex ) {
                example::Exception new_issue(ERS_HERE, ex) ;
                result = new ::ers2idl::Issue() ; // value to return
                daq::ers2idl_issue( *((ers2idl::Issue*)result), new_issue) ; // fill the fields, recursively create result
            }
        }

}; 

class Raiser: POA_example::ex_raiser {

	Raiser() {} ;
	~Raiser() {} ;
	
	void raise_ex() {
		example::Exception ers_issue(ERS_HERE) ;
		throw example::NotAllowed(daq::ers2idl_issue(ers_issue)) ;
	}
} ;

