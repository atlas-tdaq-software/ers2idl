#include <chrono>
#include <vector>
#include <string>

#include "ers/ers.h" 
#include <ers/RemoteContext.h>
#include <ers/AnyIssue.h>

#include "ers2idl/ers2idl.h"

using namespace std::chrono ;

using namespace daq ;

ers::Severity idl2ers_sev(::ers2idl::Severity idlsev) {
switch ( idlsev ) {
	case ::ers2idl::Severity::debug: 		return ers::severity::Debug; break;
	case ::ers2idl::Severity::log: 		    return ers::severity::Log; break;
	case ::ers2idl::Severity::information: 	return ers::severity::Information; break;
	case ::ers2idl::Severity::warning: 		return ers::severity::Warning; break;
	case ::ers2idl::Severity::error:		return ers::severity::Error; break;
	default: 				            	return ers::severity::Fatal;
	}
}

::ers2idl::Severity ers2idl_sev(ers::severity erssev) {
switch ( erssev ) {
	case ers::severity::Debug: 			return ::ers2idl::Severity::debug;        	break;
	case ers::severity::Log: 		    return ::ers2idl::Severity::log;          	break;
	case ers::severity::Information: 	return ::ers2idl::Severity::information;  	break;
	case ers::severity::Warning: 		return ::ers2idl::Severity::warning;      	break;
	case ers::severity::Error: 			return ::ers2idl::Severity::error;			break;
	default:							return ::ers2idl::Severity::fatal;
	}
}

::ers2idl::Issue_var daq::ers2idl_issue(const ers::Issue& issue) {
	::ers2idl::Issue_var ret = new ::ers2idl::Issue() ;
	ers2idl_issue(ret, issue) ; // Issue_var gives access to Issue*
	return ret ; 
}

void daq::_ers2idl_issue(::ers2idl::Issue* message, const ers::Issue& issue) {
	daq::ers2idl_issue(*message, issue) ;
}

void daq::ers2idl_issue(::ers2idl::Issue& message, const ers::Issue& issue) 
{
ERS_DEBUG(3, "Building message: " << issue.get_class_name()) ;
	message.ID = issue.get_class_name() ;
	message.message = issue.message().c_str() ;
	message.sev = ers2idl_sev(issue.severity()) ;
	// message.time = issue.time_t() ;
	// long long is 64bit, sufficient for microseconds
	message.time = duration_cast<microseconds>(issue.ptime().time_since_epoch()).count() ;

	auto quals = issue.qualifiers() ;
	message.quals.length(quals.size()) ;
	size_t i = 0 ;
	for ( auto a_qual: quals )
		{ message.quals[i++] = a_qual.c_str() ; }

	auto params = issue.parameters() ;
ERS_DEBUG(5, "Message has " << params.size() << " parameters") ;
	message.params.length(params.size()) ;
	i = 0 ;
	for ( auto a_param: params )
		{
ERS_DEBUG(5, "Adding parameter to message: " << a_param.first.c_str() << "=" << a_param.second.c_str()) ;
		message.params[i].name = a_param.first.c_str() ;
 		message.params[i].value = a_param.second.c_str() ;
		i++ ;
 		}

	message.cont.cwd = issue.context().cwd() ;
	message.cont.file_name = issue.context().file_name() ;
	message.cont.function_name = issue.context().function_name() ;
	message.cont.host_name = issue.context().host_name() ;
	message.cont.package_name = issue.context().package_name() ;
	message.cont.user_name = issue.context().user_name() ;
	message.cont.application_name = issue.context().application_name() ;
	message.cont.user_id = issue.context().user_id() ;
	message.cont.process_id = issue.context().process_id() ;
	message.cont.thread_id = issue.context().thread_id() ;
	message.cont.line_number = issue.context().line_number() ;

	if ( issue.cause() )
		{
		message.cause.length(1) ; // allocate cause message
ERS_DEBUG(5, "Building chained message: " << issue.cause()->get_class_name()) ;
		ers2idl_issue(message.cause[0], *issue.cause()) ; // fill it  
		}

ERS_DEBUG(5, "Completed building message") ;
ERS_DEBUG(5, "Message size: " << sizeof(message)) ;
}

std::unique_ptr<ers::Issue> daq::idl2ers_issue(const ::ers2idl::Issue& message) 
{
std::unique_ptr<ers::Issue> issue ;

std::vector<std::string> qualifiers ;
for ( size_t i = 0; i < message.quals.length(); i++ )
	{ qualifiers.push_back(std::string(message.quals[i])) ; }

ers::string_map parameters ;
for ( size_t i = 0; i < message.params.length(); i++ )
	{ parameters[std::string(message.params[i].name)] = message.params[i].value ; }

ers::RemoteContext rc(	std::string(message.cont.package_name), std::string(message.cont.file_name),
			message.cont.line_number, std::string(message.cont.function_name),
			ers::RemoteProcessContext( std::string(message.cont.host_name),
			message.cont.process_id, message.cont.thread_id, std::string(message.cont.cwd),
			message.cont.user_id, std::string(message.cont.user_name), std::string(message.cont.application_name))) ;

if ( message.cause.length() ) // there is cause issue
	{
ERS_DEBUG(3, "Building nested issue " << message.cause[0].ID) ;
	ers::Issue* cause = idl2ers_issue(message.cause[0]).release() ; // recursion, now parent issue is the owner of the cause
	issue = std::make_unique<ers::AnyIssue> (std::string(message.ID), idl2ers_sev(message.sev), rc,
			system_clock::time_point(microseconds(message.time)), std::string(message.message), qualifiers, parameters, cause) ;
	}
else
	{
	issue = std::make_unique<ers::AnyIssue> (std::string(message.ID), idl2ers_sev(message.sev), rc, 
			system_clock::time_point(microseconds(message.time)), std::string(message.message), qualifiers, parameters) ;
	}

issue->set_severity(idl2ers_sev(message.sev)) ;

ERS_DEBUG(3, "Built issue " << message.ID) ;
return issue ;
}
