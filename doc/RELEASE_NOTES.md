# ers2idl

## tdaq-11-02-00

New TDAQ package that can be used for passing ERS issues (including chained ones) declared in IDL interfaces between CORBA server and client.

It contains the ERS Issue definition in IDL file (ers2idl module) that should be included in other IDL and used in IDL interface definitions (e.g. as `_out` type of parameters for returning an Issue from a remote server). For the implementation, a helper C++ library is provided to convert IDL-generated structure into `ers::Issue` C++ object on the client side, or in the other direction from `ers::Issue` into `ers2idl::Issue` for instantiating it on the server side. Available helper functions (namespace daq):

 `daq::ers2idl_issue(::ers2idl::Issue& message, const ers::Issue& issue)`
 `std::unique_ptr<ers::Issue> daq::idl2ers_issue(const ::ers2idl::Issue& message)`
 `::ers2idl::Issue_var daq::ers2idl_issue(const ers::Issue& issue)`
