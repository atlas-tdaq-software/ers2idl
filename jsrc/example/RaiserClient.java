package example ;

/**
 * Example class that gets a CORBA reference to a remote example.ex_raiser object and calls its method catching an IDL exception
 */
public class RaiserClient {
    
    public static void main(String[] args) {
        ipc.ObjectEnumeration<example.ex_raiser> objects = null ;

        // somehow get a reference to remote ex_raiser (see jsrc/example/RaiserImpl.java and src/example.cpp for possible implementation code)
        try {
            objects = new ipc.ObjectEnumeration<example.ex_raiser>(new ipc.Partition(args[1]), example.ex_raiser.class, true) ;
        } catch ( final ipc.InvalidPartitionException ex ) {
            ers.Logger.error(ex) ;
            System.exit(1) ;
        }
        
        example.ex_raiser raiser = objects.nextElement().reference ;
    
        // call remote method raise_ex and catch its exception, convert to ers.Issue and report to ERS stream
        try {
            raiser.raise_ex() ;
        } catch ( final example.NotAllowed ex ) {
            final ers.Issue not_allowed_ex1 = ers2idl.Converter.idl2ersIssue( ex.cause ) ; // cause is the IDL NotAllowed exception attribute incapsulating data structure of ers issue
            ers.Logger.error(not_allowed_ex1) ;
            final ers.Issue not_allowed_ex2 = ers2idl.Converter.idl2ersIssue( ex.cause,  example.NotAllowedHelper.id() ) ; // override ex.cause.ID (ID of the embedded issue) with IDL issue id, like "example.NotAllowed"
            ers.Logger.error(not_allowed_ex2) ;
            final ers.Issue not_allowed_ex3 = ers2idl.Converter.idl2ersIssue( ex.cause,  ex.getClass().getName() ) ; // override ex.cause.ID (ID of the embedded issue) with IDL issue id, like "example.NotAllowed"
            ers.Logger.error(not_allowed_ex3) ;
        } catch ( final Exception ex ) { // CORBA, etc
            ers.Logger.error(ex) ;
        }
    }
}
