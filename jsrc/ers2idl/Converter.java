package ers2idl ;

import java.util.ArrayList ;
import java.util.List ;
import java.util.HashMap ;
import java.util.Map ;
import java.util.Arrays ;

public class Converter {

	/**
	 * Recursively converts ers2idl.Issue to ers.Issue
	 * NB: The ID of the constructed issue will be taken from idl_issue (not from it's parent issue thrown on the server) 
	 * - if you want to change it, use next function with two arguments
	 * 
	 * @param  idl_issue an issue received from IDL interface (as _in parameter or as exception).
	 * 
     * @throws RuntimeException incapsulating ers.BadIssue from createIssue
     *            
	 * @return ers.Issue new ERS Issue built from IDL one. @see ers.Issue
     *  
	 */
public static ers.Issue idl2ersIssue(final ers2idl.Issue idl_issue) {
	return idl2ersIssue(idl_issue, idl_issue.ID) ;
}

	/**
	 * Recursively converts ers2idl.Issue to ers.Issue
	 * 
	 * @param idl_issue an issue received from IDL interface (as _in parameter or as exception)
	 * @param ID an ID of the origial IDL issue created on server, to override ID of the cause (embedded) ERS issue
	 * 
     * @throws RuntimeException incapsulating ers.BadIssue from createIssue
     *            
	 * @return ers.Issue new ERS Issue built from IDL one. @see ers.Issue
     *  
	 */
public static ers.Issue idl2ersIssue(final ers2idl.Issue idl_issue, final String ID) {
		ers.Issue issue = null, cause = null;

		final ers.ProcessContext pcontext = new ers.ProcessContext(idl_issue.cont.host_name, idl_issue.cont.process_id,
				idl_issue.cont.thread_id, idl_issue.cont.cwd, idl_issue.cont.user_id, idl_issue.cont.user_name);
		final ers.RemoteContext rcontext = new ers.RemoteContext(idl_issue.cont.package_name, idl_issue.cont.file_name,
				idl_issue.cont.line_number, idl_issue.cont.function_name, idl_issue.cont.application_name, pcontext,
				(long)(idl_issue.time/1000) );	// ers2idl is ll in microsecs, but jers time in millis

		final List<String> qualifiers = new ArrayList<String>(Arrays.asList(idl_issue.quals));

		final Map<String, String> parameters = new HashMap<String, String>();

		for ( ers2idl.ERSParameter par : idl_issue.params ) {
			parameters.put(par.name, par.value);
		}

		if ( idl_issue.cause.length > 0 ) {
			cause = idl2ersIssue(idl_issue.cause[0]) ; // recursively build chained issue
		}

		try {
			issue = ers.Issue.createIssue(rcontext, ID, idl_issue.message, cause, idl2ersSeverity(idl_issue.sev),
					(long)(idl_issue.time/1000), qualifiers, parameters); // mts is in microsecs, but jers time in millis
		} catch (final ers.BadIssue ex) {
			// System.err.println("Failed to recreate issue of class " + idl_issue.ID + ": " + ex.getMessage());
            throw new RuntimeException("Failed to create issue of class " + idl_issue.ID, ex) ;
			// issue = new ers.Issue(ex);
		}

		return issue;
	}

	private static ers.Severity.level idl2ersSeverity(final ers2idl.Severity idl_sev) {
		switch (idl_sev.value()) {
		case ers2idl.Severity._debug:
			System.err.println("It is not expected that DEBUG ERS Issues will be transfered via MTS. Retuning rank 0.") ; // TODO Exception?
			return ers.Severity.level.Debug;
		case ers2idl.Severity._information:
			return ers.Severity.level.Information;
		case ers2idl.Severity._warning:
			return ers.Severity.level.Warning;
		case ers2idl.Severity._error:
			return ers.Severity.level.Error;
		case ers2idl.Severity._fatal:
			return ers.Severity.level.Fatal;
		case ers2idl.Severity._log:
			return ers.Severity.level.Log;
		default:
			throw new RuntimeException("Unexpected integral ers.Severity received from IDL: " + idl_sev.value());
		}
	}
 	
}