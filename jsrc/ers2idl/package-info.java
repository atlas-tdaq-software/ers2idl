/**
 * Package ers2idl should be used for passing ERS issues (including chained ones) via CORBA IDL calls. 
 * 
 * It contains the ERS Issue definition in IDL file (ers2idl module) that should be included in other IDL and used in IDL interface definitions,
 * and a helper @see ers2idl.Converter#idl2ersIssue for convertions from IDL to ERS Issue to be used on the client side
 * (no server counterpart provided as we don't have Java CORBA servers, ers2idl is implemented in C++ only).
 * 
 * <p>
 * @see ers2idl.Converter#idl2ersIssue
 * 
 */
package ers2idl ;
