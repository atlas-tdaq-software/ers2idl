# ers2idl package

## Introduction

Package can be used to help passing ERS issues (including chained ones) via CORBA IDL calls.

## Content

It contains the ERS Issue definition in IDL file (ers2idl module) that should be included in other IDL and used in IDL interface definitions (e.g. as `_out` type of parameters for returning an Issue from a remote server). For the implementation, a helper C++ library is provided to convert IDL-generated structure into `ers::Issue` C++ object on the client side, or in the other direction from `ers::Issue` into `ers2idl::Issue` for instantiating it on the server side. Available helper functions (namespace daq):

  * `daq::ers2idl_issue(::ers2idl::Issue& message, const ers::Issue& issue)`
  * `std::unique_ptr<ers::Issue> daq::idl2ers_issue(const ::ers2idl::Issue& message)`
  * `::ers2idl::Issue_var daq::ers2idl_issue(const ers::Issue& issue)`

## Examples

[idl/example.idl](./idl/example.idl) and [src/example.cpp](./src/example.cpp) demonstrate the usage.

See also `mts` package (`mts.idl` and corresponding [commit](https://gitlab.cern.ch/atlas-tdaq-software/mts/-/commit/ae7e666c267943ac769af95be8f8bcfdc6ef1bb8)).
