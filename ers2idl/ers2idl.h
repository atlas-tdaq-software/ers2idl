#include <memory>

#include <ers/Issue.h>
#include <ers2idl/ers2idl.hh>

/** \page ers2idl_package ERS to IDL package
 * 
 * Package to convert chained ERS issues into IDL ers2idl::Issue structure (see ers2idl/idl/ers2idl.idl)
 * for sending them over CORBA interface and then converting back to C++ ERS objects on the receiver side.
 * An IDL Exception (embedding cause ers::Issue as a single attribute) can also be thrown from the user code.
 *
 *  \see ers::Issue
 *  \see ers2idl::Issue
 *  \see daq::ers2idl_issue
 *  \see daq::idl2ers_issue
 * 
 * %Examples:
 * ers2idl/idl/example.idl
 * ers2idl/src/example.cpp
 */

namespace daq {

/**
 * Function for recursive construction of (chained) ers2idl::Issue from ers::Issue (the sender part).
 * message structure is assumed to be already allocated by user with its corba constructor with new (as for Issue_out).
 * \ingroup public 
 * 
 * \param message   structure generated from IDL, allocated with new and to be passed back as ::ers2idl::Issue_out type
 * \param issue     original ers::Issue
 * 
 * \return          no return value, returned is the message parameter
 */
void ers2idl_issue(::ers2idl::Issue& message, const ers::Issue& issue) ;

/** helper function with little different signature, not for public use
 * \ingroup private
 * \see ers2idl_issue
*/
void _ers2idl_issue(::ers2idl::Issue* message, const ers::Issue& issue) ;

/**
 * Function for recursive build of chained ers2idl::Issue from ers::Issue (sender part)
 * ers2idl::Issue is allocated here and returned as _var CORBA staructure
 * \ingroup public
 * 
 * \param issue     original ers::Issue
 * 
 * \return          ers2idl::Issue_var (a CORBA smart pointer to ers2idl::Issue) allocated by the function
 */
::ers2idl::Issue_var ers2idl_issue(const ers::Issue& issue) ;


/**
 * Function for recursive build of chained ers::Issue from ers2idl::Issue (receiver part)
 * \ingroup public
 * 
 * \param message   ers2idl::Issue received via CORBA
 * 
 * \return          unique pointer to created ers::Issue
*/
std::unique_ptr<ers::Issue> idl2ers_issue(const ::ers2idl::Issue& message) ;
} 
